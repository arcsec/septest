//
//  UIView.swift
//
//  Created by Armin on 9/25/18.
//  Copyright © 2018 Armin. All rights reserved.
//

import Foundation
import UIKit

extension UIView {
    
    func roundCorners(_ corners:UIRectCorner, radius: CGFloat) {
        
        let path = UIBezierPath(roundedRect: self.bounds, byRoundingCorners: corners, cornerRadii: CGSize(width: radius, height: radius))
        let mask = CAShapeLayer()
        mask.path = path.cgPath
        self.layer.mask = mask
    }
    
    func addCornerRadius(cornerSize:CGFloat) {
        
        self.layer.cornerRadius = cornerSize;
        self.layer.masksToBounds = true;
        
    }
    
    func addBorder(borderSize:CGFloat, borderColor:UIColor) {
        
        self.layer.borderWidth = borderSize;
        self.layer.borderColor = borderColor.cgColor;
        
    }
    
    func drawTimeLinePath(_ view: UIView) {
        
        let path = UIBezierPath()
        path.move(to: CGPoint(x: view.frame.width - 40, y: 0))
        path.addLine(to: CGPoint(x: frame.width - 40, y: self.frame.height))
        
        let shapeLayer = CAShapeLayer()
        shapeLayer.path = path.cgPath
        shapeLayer.strokeColor = UIColor.init(rgb: 0xdaddcd, a: 1.0).cgColor
        shapeLayer.fillColor = UIColor.clear.cgColor
        shapeLayer.lineWidth = 2
        shapeLayer.zPosition = 0
        view.layer.addSublayer(shapeLayer)
        
    }
    
    func drawTimeLineDot() {
        
        let circlePath = UIBezierPath(arcCenter: CGPoint(x: frame.width - 40,y: frame.height/2), radius: CGFloat(3), startAngle: CGFloat(0), endAngle:CGFloat(Double.pi * 2), clockwise: true)
        let cshapeLayer = CAShapeLayer()
        cshapeLayer.path = circlePath.cgPath
        //change the fill color
        cshapeLayer.fillColor = UIColor.init(rgb: 0x93bb40, a: 1.0).cgColor
        //you can change the stroke color
        cshapeLayer.fillColor = UIColor.init(rgb: 0x93bb40, a: 1.0).cgColor
        //you can change the line width
        cshapeLayer.lineWidth = 3.0
        layer.addSublayer(cshapeLayer)
    }
    
    func drawUnderLine() {
        let path = UIBezierPath()
        path.move(to: CGPoint(x: 20, y: 25))
        path.addLine(to: CGPoint(x: frame.width-20, y: 25))
        
        let shapeLayer = CAShapeLayer()
        shapeLayer.path = path.cgPath
        shapeLayer.strokeColor = UIColor.gray.cgColor
        shapeLayer.fillColor = UIColor.clear.cgColor
        shapeLayer.lineWidth = 1
        shapeLayer.zPosition = 0
        layer.addSublayer(shapeLayer)
    }
    func addDashLineBorder(dashPattern:[Int], strokeColor:UIColor) {
        
        let viewDashBorder = CAShapeLayer()
        viewDashBorder.strokeColor = strokeColor.cgColor
        viewDashBorder.lineDashPattern = dashPattern as [NSNumber] // example: [2,2]
        viewDashBorder.frame = self.bounds
        viewDashBorder.fillColor = nil
        viewDashBorder.path = UIBezierPath(rect: self.bounds).cgPath
        self.layer.addSublayer(viewDashBorder)
    }
    
    func makeCircular() {
        self.layer.cornerRadius = min(self.bounds.size.height, self.bounds.size.width) / 2.0
        //self.center = cntr
        self.clipsToBounds = true
    }
    
    func addDashedBorder(_ color:UIColor ,lineWidth:CGFloat)->CAShapeLayer {
        
        let shapeLayer:CAShapeLayer = CAShapeLayer()
        let frameSize = self.frame.size
        let shapeRect = CGRect(x: 0, y: 0, width: frameSize.width , height: frameSize.height)
        
        shapeLayer.bounds = shapeRect
        shapeLayer.position = CGPoint(x: frameSize.width/2 , y: frameSize.height/2)
        shapeLayer.fillColor = UIColor.clear.cgColor
        shapeLayer.strokeColor = color.cgColor
        shapeLayer.lineWidth = lineWidth
        shapeLayer.lineJoin = CAShapeLayerLineJoin.round
        shapeLayer.lineDashPattern = [6,3]
        shapeLayer.path = UIBezierPath(roundedRect: shapeRect, cornerRadius: 5).cgPath
        self.layer.addSublayer(shapeLayer)
        return shapeLayer
    }
    
    func putShadowOnView(_ shadowColor:UIColor, radius:CGFloat, offset:CGSize, opacity:Float)-> UIView{
        
        var shadowFrame = CGRect.zero // Modify this if needed
        shadowFrame.size.width = 0.0
        shadowFrame.size.height = 0.0
        shadowFrame.origin.x = 0.0
        shadowFrame.origin.y = 0.0
        
        let shadow = UIView(frame: shadowFrame)//[[UIView alloc] initWithFrame:shadowFrame];
        shadow.isUserInteractionEnabled = false; // Modify this if needed
        shadow.layer.shadowColor = shadowColor.cgColor
        shadow.layer.shadowOffset = offset
        shadow.layer.shadowRadius = radius
        shadow.layer.masksToBounds = false
        shadow.clipsToBounds = false
        shadow.layer.shadowOpacity = opacity
        self.superview?.insertSubview(shadow, belowSubview: self)
        shadow.addSubview(self)
        return shadow
    }
    
}

