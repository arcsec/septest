//
//  Localizable.swift
//
//  Created by Armin on 1/21/21.
//

import Foundation

//
//static let your_gallery                         =   "your_gallery".localized
//static let your_gallery_desc                         =   "your_gallery".localized


extension String {

    var localized: String {
        
        let language = "".currentLanguage
        
        switch language {
        case "fa":
            switch self {
            
            case "search_result_empty":
                return "نتیجه‌ای یافت نشد"
            
            case "events":
                return "رویداد‌ها"
                
            case "title":
                return "عنوان"
            
            case "change_phone":
                return "تغییر شماره تلفن"
                
            case "major_update":
                return "بروزرسانی مهم و فوری"
                
            case "major_update_description":
                return "بروزرسانی سریع"
                
            case "minor_update":
                return "بروز‌رسانی "
                
            case "minor_update_description":
                return "نسخه جدیدی ارائه شده است"
                
            case "update_alert":
                return "هشدار بروز‌رسانی"
                
            case "skip_update":
                return "فعلا نه!"

                
            
            case "oscar":
                return "اسکار"
                
            case "bafta":
                return "بفتا"
            
            case "golden_globe":
                return "گلدن گلوب"
                
                
            case "duration":
                return "مدت زمان"
                
            case "year":
                return "سال تولید"
                
            case "language":
                return "زبان"
                
            case "age_rate":
                return "محدودیت سنی"
                
            case "director":
                return "کارگردان"
                
            case "actors":
                return "بازیگران"
                
            case "writer":
                return "نویسنده"
                
            case "producers":
                return "تهیه کنندگان"
                
            
            case "Please_enter_your_phone_number":
                return "لطفا شماره تلفن همراه خود را وارد نمائید."
                
            case "Send_Verification_Code":
                return "ارسال کد فعال سازی"
                
            case "activation_code_is_invalid":
                return "کد فعال‌ سازی را اصلاح کنید"
                
            case "Please_enter_valid_password":
                return "رمز عبور شما باید شامل حروف بزرگ، حروف کوچک، عدد و کاراکتر های ویژه و حداقل به طول ۸ باشد"
                
            case "Please_enter_password":
                return "رمز عبور را وارد کنید"
                
            case "Please_enter_a_valid_email":
                return "ایمیل خود را وارد نمایید"
                
            case "Your_email_is_invalid_please_enter_again":
                return "ایمیل شما معتبر نمی‌باشد"
                
            case "InternetConnectionError":
                return "خطای دسترسی به اینترنت"
                
            case "ServerErro":
                return "خطای سرور"
                
            case "Timeout":
                return "عدم برقراری ارتباط"
                
            case "please_wait_for_activation_code":
                return "تا در یافت کد فعال‌سازی صبر کنید"
                
            case "send_again":
                return "ارسال دوباره"
                
            case "change_phone_number":
                return "تغییر شماره همراه"
                
            case "login":
                return "ورود"
                
            case "login_description":
                return "برای ورود نام کاربری و رمز عبور خود را وارد نمایید"
                
            case "reset_password":
                return "بازنشانی رمز عبور"
                
            case "dont_signup":
                return "ثبت نام نکرده ام"
                
            case "email_or_username":
                return "ایمیل یا نام کاربری"
                
            case "password":
                return "رمز عبور"
                
            case "password_retype":
                return "تکرار رمز عبور"
                
            case "reset_password_description":
                return "برای بازنشانی رمز عبور ایمیل خود را وارد نمایید"
                
            case "verify_code_description":
                return "برای تغییر رمز عبور کد تایید ارسال شده به ایمیل خود را در قسمت زیر وارد نمایید"
                
            case "verify_code":
                return "کد تأیید"
                
            case "dont_resieve_verification_code":
                return "کد را دریافت نکرده ام"
                
            case "new_password":
                return "رمز عبور جدید"
                
            case "new_password_description":
                return "لطفا رمز عبور جدید خود را وارد نمایید"
                
            case "signup":
                return "ثبت نام"
                
            case "signup_description":
                return "برای ثبت نام فرم زیر را تکمیل کنید."
                
            case "name":
                return "نام"
                
            case "username":
                return "نام کاربری"
                
            case "email":
                return "ایمیل"
                
            case "signup_befor_login":
                return "قبلا ثبت نام کرده ام. ورود"
                
            case "change_email":
                return "تغییر ایمیل"
                
            case "search":
                return "جستجو"
                
            case "profile":
                return "پروفایل"
                
            case "watch_list":
                return "لیست تماشا"
                
            case "downloads":
                return "دانلود‌ها"
                
            case "watch_list_desc":
                return "ویدیو های که به لیست اضافه کرده اید"
                
            case "downloads_desc":
                return "ویدیو های دانلود شده را در این قسمت ببینید"
                
            case "notifications":
                return "اعلان‌ها"
                
            case "recieve_notifications":
                return "دریافت اعلان ها"
                
            case "revcieve_programs_notifications":
                return "دریافت اعلان زمان پخش"
                
            case "revcieve_programs_notifications_desc":
                return "۵ دقیقه قبل از شروع فیلم ها اعلان جدید دریافت میکنید"
                
            case "recieve_nececery_notifications":
                return "دریافت اعلان ضروری"
                
            case "recieve_nececery_notifications_description":
                return "تغییر فرکانس و اخبار مهم در مورد شبکه"
                
            case "recieve_events_notifications":
                return "رویداد ها"
                
            case "recieve_events_notifications_desc":
                return "دریافت اعلان برای رویداد های مهم و جشنواره ها"
                
            case "play_form":
                return "حالت پخش"
                
            case "automatic_play":
                return "پخش خودکار"
                
            case "automatic_play_description":
                return "پخش فیلم یا سریال بعدی پس از اتمام"
                
            case "saving":
                return "دخیره سازی"
                
            case "saving_desc":
                return "پاک کردن دانلودشده ها پس از تماشا"
                
            case "festivals":
                return "جشنواره‌ها"
                
            case "promote":
                return "تبلیغات"
                
            case "cinefilm":
                return "سینه فیلم"
                
            case "cineseries":
                return "سینه سریال"
                
            case "yesterday":
                return "دیروز"
                
            case "today":
                return "امروز"
                
            case "tomorrow":
                return "فردا"
                
            case "dayaftertomorrow":
                return "پس فردا"
                
            case "programs":
                return "برنامه‌ها"
                
            case "description":
                return "توضیحات"
                
            case "comments":
                return "نظرات"
                
            case "movie":
                return "فیلم"
                
            case "serial":
                return "سریال"
                
            case "timeline":
                return "جدول پخش"
                
            case "schedule":
                return "برنامه هفتگی"
                
            case "news":
                return "اخبار"
                
            case "live":
                return "پخش زنده"
                
            case "reminders":
                return "یادآور‌ها"
                
            case "reminders_desc":
                return "یادآوری‌های متعلق به شما"
                
            case "your_gallery":
                return "گالری شما"
                
            case "your_gallery_desc":
                return "ویدیو‌های آپلود شده توسط شما"
                
            case "awards":
                return "جوایز"
                
            case "awards_desc":
                return "جوایز و قرعه‌کشی"
                
            case "men":
                return "مرد"
                
            case "women":
                return "زن"
                
            case "setting":
                return "تنظیمات"
                
            case "user_profile":
                return "اطلاعات کاربری"
                
            case "edit_profile":
                return "ویرایش پروفایل"
                
            case "all":
                return "همه"
                
            case "change_language":
                return "تغییر زبان"
                
            case "download":
                return "دانلود"
                
            case "bookmark":
                return "ذخیره"
                
            case "sharing":
                return "اشتراک گذاری"
                
            case "current_amount":
                return ":امتیاز فعلی"
                
            case "playing":
                return "در حال پخش"
                
            case "please_search":
                return "جستجو کنید"
                
                
            case "brows_all":
                return "مشاهده همه"
                
            case "logout":
                return "خروج از حساب کاربری"
                
            case "mobile":
                return "شماره همراه"
                
            case "change_profile_pic":
                return "تغییر نمایه"
                
            case "birthdate":
                return "تاریخ تولد"
                
            case "sex":
                return "جنسیت"
                
            case "country":
                return "کشور"
                
            case "current_password":
                return "رمز عبور فعلی"
                
            case "confirm_new_password":
                return "تکرار رمز عبور جدید"
                
            case "save_changes":
                return "ذخیره تغییرات"
                
            case "cancel":
                return "انصراف"
                
            case "mobile_or_username":
                return "شماره همراه یا نام کاربری"
                
            case "get_verification_code":
                return "دریافت کد تایید"
                
            case "i_want_to_login":
                return "می‌خواهم وارد شوم"
                
            case "change_password":
                return "تغییر رمز عبور"
                
            case "send_verification_code":
                return "ارسال کد تایید"
                
            case "verify_code_description_mobile":
                return "برای تغییر رمز عبور کد تایید ارسال شده به شماره همراه خود را در قسمت زیر وارد نمایید"
                
            case "an_error_occoured":
                return "خطایی رخ داده است"
                
            case "done":
                return "انجام شد"
                
            case "you_logged_out":
                return "شما از حساب کاربری تان خارج شده‌اید..."
                
            case "minutes":
                return "دقیقه"
                
            case "add_comment":
                return "افزودن نظر..."
                
            case "hour":
                return "ساعت"
                
            case "upload":
                return "آپلود"
                
            case "choose_video":
                return "انتخاب ویدیو"
                
            case "login_first":
                return "لطفا ابتدا وارد شوید"
                
                
            case "default_language_changes_description":
                return "به منظور تجربه کاربری بهتر، اپلیکیشن را بسته و دوباره اجرا نمایید."
                
            default:
                return ""
            }
            
            
            
            
        case "en":
            
            switch self {
            
            case "search_result_empty":
                return "No results found"
            
            case "events":
                return "Events"
                
            case "title":
                return "Title"
                
            case "change_phone":
                return "Change Phone Number"
            
            case "major_update":
                return "An Important Update Does Exist."
                
            case "major_update_description":
                return "Update right now!"
                
            case "minor_update":
                return "Update"
                
            case "minor_update_description":
                return "A newer version Does Exist."
                
            case "update_alert":
                return "Update Alert...!"
                
            case "skip_update":
                return "Skip this!"
            
            
            case "oscar":
                return "Oscar"
                
            case "bafta":
                return "Bafta"
            
            case "golden_globe":
                return "Golden Globe"
            
            
            case "duration":
                return "Duration"
                
            case "year":
                return "Year"
                
            case "language":
                return "Language"
                
            case "age_rate":
                return "Age Rate"
                
            case "director":
                return "Director"
                
            case "actors":
                return "Actors"
                
            case "writer":
                return "Writer"
                
            case "producers":
                return "Producers"
                
            case "Please_enter_your_phone_number":
                return "Plaease enter your phone number"
                
            case "Send_Verification_Code":
                return "Send Activation Code"
                
            case "activation_code_is_invalid":
                return "Activation Code is Invalid"
            case "Please_enter_valid_password":
                return "Your password must contain at least one special character, one Uppercase characters, also be at least 8 characters long"
                
            case "Please_enter_password":
                return "Please Enter Password"
                
            case "Please_enter_a_valid_email":
                return "Please Enter your Email"
                
            case "Your_email_is_invalid_please_enter_again":
                return "Email is not valid"
                
            case "InternetConnectionError":
                return "Internet Connection Error!"
                
            case "ServerErro":
                return "Server Error"
                
            case "Timeout":
                return "Request Timeout"
                
            case "please_wait_for_activation_code":
                return "Actiovation Code Sent"
                
            case "send_again":
                return "Send Again"
                
            case "change_phone_number":
                return "Change Phone Number"
                
            case "login":
                return "Login"
                
            case "login_description":
                return "Enter username and password to login"
                
            case "reset_password":
                return "Reset Password"
                
            case "dont_signup":
                return "Signup"
                
            case "email_or_username":
                return "Email or Username"
                
            case "password":
                return "Password"
                
            case "password_retype":
                return "Confirm Password"
                
            case "reset_password_description":
                return "Enter your Email to Reset Password"
                
            case "verify_code_description":
                return "Enter verification code"
                
            case "verify_code":
                return "Verifification Code"
                
            case "dont_resieve_verification_code":
                return "Don't Recieve Verification Code"
                
            case "new_password":
                return "New Password"
                
            case "new_password_description":
                return "New Password"
                
            case "signup":
                return "Signup"
                
            case "signup_description":
                return "Please fill all fields for signing up"
                
            case "name":
                return "Name"
              
            case "username":
                return "Username"
                
            case "email":
                return "Email"
                
            case "signup_befor_login":
                return "Signup before! Login..."
                
            case "change_email":
                return "Change Email"
                
            case "search":
                return "Search"
                
            case "profile":
                return "Profile"
                
            case "watch_list":
                return "Watch List"
                
            case "downloads":
                return "Download List"
                
            case "watch_list_desc":
                return "All Videos added to watch list"
                
            case "your_gallery":
                return "Your Gallery"
                
            case "your_gallery_desc":
                return "All Videos you are upload"
                
            case "downloads_desc":
                return "All Videos you are download"
                
            case "notifications":
                return "Notifications"
                
            case "recieve_notifications":
                return "Recieve Notifications"
                
            case "revcieve_programs_notifications":
                return "Revcieve Programs Notifications"
                
            case "revcieve_programs_notifications_desc":
                return "You recieve notification 5 minutes before starting movie"
                
            case "recieve_nececery_notifications":
                return "Recieve Nececery Notifications"
                
            case "recieve_nececery_notifications_description":
                return "Change frequency and importatnt events"
                
            case "recieve_events_notifications":
                return "Events"
                
            case "recieve_events_notifications_desc":
                return "Recieve notifications about channel and events"
                
            case "play_form":
                return "Play Format"
                
            case "automatic_play":
                return "Auto Play"
                
          
                
            case "automatic_play_description":
                return "Play Next Movie Immediately"
                
            case "saving":
                return "Saving"
                
            case "saving_desc":
                return "Removed downloaded movie after watching"
                
            case "festivals":
                return "Festivals"
                
            case "promote":
                return "Promotion"
                
            case "cinefilm":
                return "Cine Film"
                
            case "cineseries":
                return "Cine Series"
                
            case "yesterday":
                return "Yesterday"
                
            case "today":
                return "Today"
                
            case "tomorrow":
                return "Tomorrow"
                
            case "dayaftertomorrow":
                return "Day after tomorrow"
                
            case "programs":
                return "Programs"
                
            case "description":
                return "Description"
                
            case "comments":
                return "Comments"
                
            case "movie":
                return "Movie"
                
            case "serial":
                return "Serial"
                
            case "timeline":
                return "Timeline"
                
            case "schedule":
                return "Weekly Schedule"
                
            case "news":
                return "News"
                
            case "live":
                return "Live"
                
            case "reminders":
                return "Reminders"
                
            case "reminders_desc":
                return "Specific Reminders for You"
                
            case "awards":
                return "Awards"
                
            case "awards_desc":
                return "Awards and lottory"
                
            case "men":
                return "Men"
                
            case "women":
                return "Women"
                
            case "setting":
                return "Setting"
                
            case "user_profile":
                return "User Profile"
                
            case "edit_profile":
                return "Edit Profile"
                
            case "all":
                return "All"
                
            case "change_language":
                return "Change Language"
                
            case "download":
                return "Download"
                
            case "bookmark":
                return "Bookmark"
                
            case "sharing":
                return "Sharing"
                
            case "current_amount":
                return "Current Score:"
                
            case "playing":
                return "Playing"
                
            case "please_search":
                return "Search"
                
                
            case "brows_all":
                return "Brows All"
                
            case "logout":
                return "Logout"
                
            case "mobile":
                return "Mobile"
                
            case "change_profile_pic":
                return "Change Profile Picture"
                
            case "birthdate":
                return "Birthdate"
                
            case "sex":
                return "Sex"
                
            case "country":
                return "Country"
                
            case "current_password":
                return "Current Password"
                
            case "confirm_new_password":
                return "Confirm New Password"
                
            case "save_changes":
                return "Save Changes"
                
            case "cancel":
                return "Cancel"
                
            case "mobile_or_username":
                return "Mobile or Username"
                
            case "get_verification_code":
                return "Get Verification Code"
                
            case "i_want_to_login":
                return "Login"
                
            case "change_password":
                return "Change Password"
                
            case "send_verification_code":
                return "Send Code"
  
            case "verify_code_description_mobile":
                return "Enter sent code for changing current password"
                
            case "an_error_occoured":
                return "An Error Occoured"
                
            case "done":
                return "Done"
                
            case "you_logged_out":
                return "You've been logged out"
                
            case "minutes":
                return "Minutes"
                
            case "add_comment":
                return "Add Comment"
                
            case "hour":
                return " - "
                
            case "upload":
                return "Upload"
                
            case "choose_video":
                return "Choose video"
                
            case "login_first":
                return "Please login first"
                
                
            case "default_language_changes_description":
                return "For better user exprience, please terminate the app and run it again."
                
            default:return ""
            }
            
        default:
            return ""
        }
        
        
        
    }
    
}
