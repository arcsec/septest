//
//  Font.swift
//
//  Created by Armin on 9/25/18.
//  Copyright © 2018 Armin. All rights reserved.
//

import Foundation
import UIKit


extension UIFont {

    static let YekanBold = UIFont(name: "IRANYekanMobile-Bold", size: UIFont.labelFontSize)!
    static let YekanRegular = UIFont(name: "IRANYekanMobile", size: UIFont.labelFontSize)!
    static let YekanLight = UIFont(name: "IRANYekanMobile-Light", size: UIFont.labelFontSize)!
    static let YekanExtraBold = UIFont(name: "IRANYekanMobile-ExtraBold", size: UIFont.labelFontSize)!
    static let YekanExtraBlack = UIFont(name: "IRANYekanMobile-ExtraBlack", size: UIFont.labelFontSize)!
    static let YekanMedium = UIFont(name: "IRANYekanMobile-Medium", size: UIFont.labelFontSize)!
    static let YekanThin = UIFont(name: "IRANYekanMobile-Thin", size: UIFont.labelFontSize)!
    static let YekanBlack = UIFont(name: "IRANYekanMobile-Black", size: UIFont.labelFontSize)!
    static let Roboto_Regular = UIFont(name: "Roboto-Regular", size: UIFont.labelFontSize)!
    static let Roboto_Bold = UIFont(name: "Roboto-Bold", size: UIFont.labelFontSize)!

    
    
    
}


class FontExtension
{
    class func getFontNames()
    {
        for family in UIFont.familyNames.sorted() {
             let names = UIFont.fontNames(forFamilyName: family)
             print("Family: \(family) Font names: \(names)")
         }
    }
}
