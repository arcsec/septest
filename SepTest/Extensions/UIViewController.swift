//
//  UIViewController.swift
//
//  Created by Sajjad Aboutalebi on 10/3/18.
//  Copyright © 2018 Armin. All rights reserved.
//

import UIKit
extension UIViewController {
    
    
    @objc func injected() {
        for subview in self.view.subviews {
            subview.removeFromSuperview()
        }
        
        viewDidLoad()
    }
    
}
