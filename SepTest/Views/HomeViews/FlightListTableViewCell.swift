//
//  FlightListTableViewCell.swift
//  SepTest
//
//  Created by NGN on 3/5/22.
//

import UIKit

class FlightListTableViewCell: UITableViewCell {
    
    var data: FlightObject? {
        didSet {
            self.name.text = "\(data?.name ?? "HEATHROW") - \(data?.address?.cityName ?? "LONDON")"
            
            self.distance.text = "\(data?.distance?.value ?? 11) \(data?.distance?.unit ?? "KM")"
            
            self.iata.text = data?.iataCode
            
            self.timezone.text = data?.timeZoneOffset
        }
        
    }
    
    @IBOutlet var name: UILabel!
    @IBOutlet var iata: UILabel!
    @IBOutlet var timezone: UILabel!
    @IBOutlet var distance: UILabel!
    @IBOutlet var containerView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        self.containerView.addCornerRadius(cornerSize: 10)
        self.containerView.addBorder(borderSize: 5, borderColor:grayG600)

    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
