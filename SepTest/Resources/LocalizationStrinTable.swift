//
//  LocalizationStrinTable.swift
//  CashBox
//
//  Created by Armin on 9/17/18.
//  Copyright © 2018 Armin. All rights reserved.
//

import Foundation

//MARK: - LocalizedString
extension String {
    
    static let internetConnectionErroMessage =  "InternetConnectionError".localized
    static let serverReachabilityErroMessage =  "ServerErro".localized
    static let timeOutConnectionErroMessage  =   "Timeout".localized
    static let loadingRequestMessage         =   "Loading".localized
    
    static let change_phone                         =   "change_phone".localized

    
    static let phoneTextfieldisEmpty         =   "PhoneIsEmpty".localized
    static let codeTextfieldisEmpty          =   "CodeIsEmpty".localized
    static let nameIsEmpty                   =   "NameISEmpty".localized
    static let login                         =   "login".localized
    static let login_description                         =   "login_description".localized
    static let reset_password                         =   "reset_password".localized
    static let dont_signup                         =   "dont_signup".localized
    static let email_or_username                         =   "email_or_username".localized
    static let password                         =   "password".localized
    static let reset_password_description                         =   "reset_password_description".localized
    static let verify_code_description                         =   "verify_code_description".localized
    static let verify_code                         =   "verify_code".localized
    static let dont_resieve_verification_code                         =   "dont_resieve_verification_code".localized
    
    static let new_password                         =   "new_password".localized
    static let new_password_description                         =   "new_password_description".localized
    static let signup                         =   "signup".localized
    static let signup_description                         =   "signup_description".localized
    static let name                         =   "name".localized
    static let username                         =   "username".localized
    static let email                         =   "email".localized
    static let signup_befor_login                         =   "signup_befor_login".localized
    static let change_email                         =   "change_email".localized
    static let password_retype                         =   "password_retype".localized
    
    static let search                         =   "search".localized
    static let profile                         =   "profile".localized
    static let upload                         =   "upload".localized
    
    static let watch_list                         =   "watch_list".localized
    static let downloads                         =   "downloads".localized
    static let watch_list_desc                         =   "watch_list_desc".localized
    static let downloads_desc                         =   "downloads_desc".localized
    
    static let notifications                         =   "notifications".localized
    static let recieve_notifications                         =   "recieve_notifications".localized
    static let revcieve_programs_notifications                         =   "revcieve_programs_notifications".localized
    static let revcieve_programs_notifications_desc                         =   "revcieve_programs_notifications_desc".localized
    static let recieve_nececery_notifications                         =   "recieve_nececery_notifications".localized
    static let recieve_nececery_notifications_description                         =   "recieve_nececery_notifications_description".localized
    static let recieve_events_notifications                         =   "recieve_events_notifications".localized
    static let recieve_events_notifications_desc                         =   "recieve_events_notifications_desc".localized
    static let play_form                         =   "play_form".localized
    static let automatic_play                         =   "automatic_play".localized
    static let automatic_play_description                         =   "automatic_play_description".localized
    static let saving                         =   "saving".localized
    static let saving_desc                         =   "saving_desc".localized
    
    static let festivals                         =   "festivals".localized
    static let promote                         =   "promote".localized
    
    static let cinefilm                         =   "cinefilm".localized
    static let cineseries                         =   "cineseries".localized
    
    static let yesterday                         =   "yesterday".localized
    static let today                         =   "today".localized
    static let tomorrow                         =   "tomorrow".localized
    static let dayaftertomorrow                         =   "dayaftertomorrow".localized
    static let programs                         =   "programs".localized
    static let events                         =   "events".localized

    static let description                         =   "description".localized
    static let comments                         =   "comments".localized

    static let movie                         =   "movie".localized
    static let serial                         =   "serial".localized
    
    static let timeline                         =   "timeline".localized
    static let schedule                         =   "schedule".localized
    

    static let news                         =   "news".localized
    static let live                         =   "live".localized

    static let reminders                         =   "reminders".localized
    static let reminders_desc                         =   "reminders_desc".localized
    static let awards                         =   "awards".localized
    static let awards_desc                         =   "awards_desc".localized

    static let your_gallery                         =   "your_gallery".localized
    static let your_gallery_desc                         =   "your_gallery".localized
    
    static let men                         =   "men".localized
    static let women                         =   "women".localized
    static let setting                         =   "setting".localized
    static let user_profile                         =   "user_profile".localized
    static let edit_profile                         =   "edit_profile".localized
    static let all                         =   "all".localized
    static let change_language                         =   "change_language".localized
    static let download                         =   "download".localized
    static let bookmark                         =   "bookmark".localized

    static let sharing                         =   "sharing".localized
    static let current_amount                         =   "current_amount".localized

    
    static let playing                         =   "playing".localized


    static let please_search                         =   "please_search".localized

    
    static let brows_all                         =   "brows_all".localized

    static let logout                         =   "logout".localized

    
    static let mobile                         =   "mobile".localized
    static let change_profile_pic                         =   "change_profile_pic".localized
    static let birthdate                         =   "birthdate".localized
    static let sex                         =   "sex".localized
    static let country                         =   "country".localized

    static let current_password                         =   "current_password".localized
    static let confirm_new_password                         =   "confirm_new_password".localized

    
    static let cancel                         =   "cancel".localized
    static let save_changes                         =   "save_changes".localized
    static let mobile_or_username                         =   "mobile_or_username".localized
    static let get_verification_code                         =   "get_verification_code".localized
    static let i_want_to_login                         =   "i_want_to_login".localized

    static let change_password                         =   "change_password".localized
    static let send_verification_code                         =   "send_verification_code".localized
    
    static let an_error_occoured                         =   "an_error_occoured".localized
    static let done                         =   "done".localized
    static let you_logged_out                         =   "you_logged_out".localized

    static let minutes                         =   "minutes".localized

    static let add_comment                         =   "add_comment".localized

    static let hour                         =   "hour".localized

    static let duration                         =   "duration".localized
    static let year                         =   "year".localized
    static let language                         =   "language".localized
    static let age_rate                         =   "age_rate".localized
    static let director                         =   "director".localized
    static let actors                         =   "actors".localized
    static let writer                         =   "writer".localized
    static let producers                        =   "producers".localized

    static let title                         =   "title".localized

    
    static let oscar                         =   "oscar".localized
    static let bafta                         =   "bafta".localized
    static let golden_globe                         =   "golden_globe".localized
    
    static let major_update                         =   "major_update".localized

    static let major_update_description                         =   "major_update_description".localized

    static let minor_update                         =   "minor_update".localized

    static let minor_update_description                         =   "minor_update_description".localized

    static let update_alert                         =   "major_update".localized

    static let skip_update                         =   "skip_update".localized
    
    static let choose_video                         =   "choose_video".localized
    
    static let login_first_desc                         =   "login_first".localized

    
    static let search_result_empty                         =   "search_result_empty".localized
    
    
    static let default_language_changes_description = "default_language_changes_description".localized


}




