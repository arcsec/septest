//
//  HomeViewController.swift
//  SepTest
//
//  Created by Armin on 3/4/22.
//

import UIKit
import GooglePlaces
import SwiftyDrop
import SwiftyJSON

class HomeViewController: UIViewController {
    
    @IBOutlet var tableView: UITableView!
    var currentLocation: CLLocation?
    var locationManager: CLLocationManager?
    var flightsList : [FlightObject]?
    var sortType: String?
    let btnRightMenu: UIButton = UIButton()
    var picker: UIPickerView = UIPickerView.init()
    var ghostTextField: UITextField = UITextField()
    var canRefresh = true
    var isLoadingList : Bool = false
    var lastContentOffset: CGFloat = 0
    
    var pageNumber:Int = 0
    var tempFlightsList : [FlightObject]?

    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        
        self.locationManager = CLLocationManager()
        self.locationManager?.desiredAccuracy = kCLLocationAccuracyBest
        self.locationManager?.requestAlwaysAuthorization()
        self.locationManager?.startUpdatingLocation()
        
        self.tableView = BaseTableView<UITableViewCell>()
        self.tableView.register(FlightListTableViewCell.self, forCellReuseIdentifier: "createMemberCell")
        self.tableView.delegate = self
        self.tableView.dataSource = self
            
        picker.delegate = self
        picker.dataSource = self
        
        self.overrideLeftButton()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.navigationItem.title = "Home"
        self.locationManager?.delegate = self
    }
    
    func overrideLeftButton(){
        
        self.btnRightMenu.setImage(UIImage(named: "sort-filter"), for: .normal)
        self.btnRightMenu.frame = CGRect.init(x: 0, y: 0, width: 30, height: 30) //CGRectMake(0, 0, 30, 30)
        
        self.ghostTextField.frame =  CGRect.init(x: 0, y: 0, width: 30, height: 30)
        self.ghostTextField.text = ""
        self.ghostTextField.backgroundColor = .clear
        self.ghostTextField.inputView = self.picker
        self.ghostTextField.tintColor = .clear
    
        self.btnRightMenu.addSubview(self.ghostTextField)
        self.btnRightMenu.bringSubviewToFront(self.ghostTextField)
        
        let rightBarButton = UIBarButtonItem(customView: self.btnRightMenu)
        self.navigationItem.rightBarButtonItem = rightBarButton
    }
    
    func getData()
    {
        BasicViewModel.shared.getFlightsData(latitude: self.currentLocation?.coordinate.latitude, longitude: self.currentLocation?.coordinate.longitude, radius: 500, pageLimit: 20, pageOffset: self.pageNumber, sort: self.sortType) { success, data in
            
            
            
            if success
            {
                self.flightsList = data as? [FlightObject]
                
                if self.pageNumber > 0
                {
                    self.tempFlightsList = self.flightsList
                    self.tempFlightsList?.append(contentsOf: self.flightsList ?? [FlightObject]())
                    self.flightsList = self.tempFlightsList
                }
                
                self.tableView.reloadData()
            }
            else
            {
                let errorObj:ErrorObject? = data as? ErrorObject
                let errorMsg:String = "Error \(errorObj?.status ?? 0) - \(errorObj?.title ?? "") (\(errorObj?.detail ?? ""))"
                Drop.down(errorMsg , state: CustomDrop.failure)
            }
        }
    }
}

// MARK: - UITableViewDelegate
extension HomeViewController:UITableViewDelegate
{
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        return
    }
}
// MARK: - UITableViewDataSource
extension HomeViewController:UITableViewDataSource
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.flightsList?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = FlightListTableViewCell()
        cell.data = self.flightsList?[indexPath.row]
        
        cell.preservesSuperviewLayoutMargins = false;
        cell.separatorInset = UIEdgeInsets.zero;
        cell.layoutMargins = UIEdgeInsets.zero;
        cell.selectionStyle = UITableViewCell.SelectionStyle.none;
        cell.separatorInset = UIEdgeInsets(top: 0.0, left: 0.0, bottom: 0.0, right: 0.0);
        cell.layoutIfNeeded()
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return 150
    }
    
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView)
    {
        self.lastContentOffset = scrollView.contentOffset.y
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
        if scrollView.contentOffset.y < -50 { //change 100 to whatever you want
            
            if canRefresh {
                
                self.canRefresh = false
                self.tableView.scrollsToTop = false
            }
        }
            
        else {
            
            self.canRefresh = true
        }
        
        
        let  height = scrollView.frame.size.height
        let contentYoffset = scrollView.contentOffset.y
        let distanceFromBottom = scrollView.contentSize.height - contentYoffset
        
        if distanceFromBottom < height && !self.isLoadingList
        {
            
            self.isLoadingList = true
            self.pageNumber += 1
            self.getData()
            
            
        }
        
        
    }
    
}

// MARK: - UIPickerViewDelegate
extension HomeViewController:UIPickerViewDelegate {
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        
        let str:String = "\(SortTypes.all[row])"
        return str
    }
    
    func pickerView(_ pickerView: UIPickerView, attributedTitleForRow row: Int, forComponent component: Int) -> NSAttributedString? {
        
        let title = String(row)
        let attributedString = NSAttributedString(string: title, attributes: [NSAttributedString.Key.foregroundColor: charcoalGreyTwo])
        
        pickerView.backgroundColor = UIColor.gray
        return attributedString
    }
    
    func pickerView(_ pickerView: UIPickerView, viewForRow row: Int, forComponent component: Int, reusing view: UIView?) -> UIView {
        
        var label = view as! UILabel?
        if label == nil {
            label = UILabel()
        }
        
        let data = SortTypes.all[row]
        let title = NSAttributedString(string: "\(data)", attributes: [NSAttributedString.Key.font: UIFont.YekanMedium.withSize(16)])
        
        label?.attributedText = title
        label?.textAlignment = .center
        return label!
        
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        if component == 0 {
            self.sortType = "\(SortTypes.all[row])"
            self.getData()
        }
    }
}

// MARK: - UIPickerViewDataSource
extension HomeViewController:UIPickerViewDataSource {
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int
    {
        return SortTypes.all.count
    }
}

// MARK: - CLLocationManagerDelegate
extension HomeViewController:CLLocationManagerDelegate {
    
    // Handle incoming location events.
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let location: CLLocation = locations.last!
        self.currentLocation = location
        self.getData()
    }
    
    // Handle authorization for the location manager.
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        
        switch status {
        case .restricted:
            print("Location access was restricted.")
        case .denied:
            print("User denied access to location.")
        case .notDetermined:
            print("Location status not determined.")
        case .authorizedAlways: fallthrough
        case .authorizedWhenInUse:
            print("Location status is OK.")
        default:
            print("Location status is OK.")
            
        }
    }
    
    // Handle location manager errors.
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print("Error: \(error)")
    }
}

