//
//  TabBarViewController.swift
//
//  Created by Armin on 10/1/20.
//  Copyright © 2020 Armin@Armin. All rights reserved.
//

import UIKit

class TabBarViewController: UITabBarController{
    
    @IBInspectable var defaultIndex: Int = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.selectedIndex = defaultIndex
        
        UITabBar.appearance().backgroundColor = almostBlack
        UITabBar.appearance().backgroundImage = UIImage()
        
        let homeStoryBoard:UIStoryboard = UIStoryboard.init(name: "Home", bundle: nil)
        
        
        let homeNavVC:NavigationViewController = homeStoryBoard.instantiateViewController(withIdentifier: "HomeTabInitNavVC") as! NavigationViewController;
        
        
        let statStr = UserData.shared.accessToken
        
        if statStr != nil && statStr != "" {
            
            self.setViewControllers(NSArray.init(objects: homeNavVC) as? [UIViewController], animated: false)
            
        }
        
        else {
            self.setViewControllers(NSArray.init(objects: homeNavVC) as? [UIViewController], animated: false)
            
        }
        
        
        let tabBarr:UITabBar = self.tabBar
        
        
        let tabBarItem1:UITabBarItem = tabBarr.items![0]
        
        tabBarItem1.selectedImage = UIImage.init(named: "62-62 home icon ios")?.withRenderingMode(UIImage.RenderingMode.alwaysOriginal)
        tabBarItem1.image = UIImage.init(named: "62-62home icon unselect ios")?.withRenderingMode(UIImage.RenderingMode.alwaysOriginal)
        tabBarItem1.title = ""
    
        
        self.selectedIndex = 0
        
        
    }
    
}


extension TabBarViewController:UITabBarControllerDelegate
{
    
    func tabBarController(_ tabBarController: UITabBarController, shouldSelect viewController: UIViewController) -> Bool {
        
        let tabViewControllers = tabBarController.viewControllers!
        let fromView = tabBarController.selectedViewController!.view
        let toView = viewController.view
        
        if (fromView == toView) {
            return false
        }
        
        let fromIndex = tabViewControllers.firstIndex(of: tabBarController.selectedViewController!)
        let toIndex = tabViewControllers.firstIndex(of: viewController)
        
        let offScreenRight = CGAffineTransform(translationX: (toView?.frame.width)!, y: 0)
        let offScreenLeft = CGAffineTransform(translationX: -(toView?.frame.width)!, y: 0)
        
        // start the toView to the right of the screen
        
        
        if (toIndex! < fromIndex!) {
            toView?.transform = offScreenLeft
            fromView?.transform = offScreenRight
        } else {
            toView?.transform = offScreenRight
            fromView?.transform = offScreenLeft
        }
        
        fromView?.tag = 124
        toView?.addSubview(fromView!)
        
        self.view.isUserInteractionEnabled = false
        UIView.animate(withDuration: 0.5, delay: 0.0, usingSpringWithDamping: 1, initialSpringVelocity: 0, options: UIView.AnimationOptions.curveEaseOut, animations: {
            
            toView?.transform = CGAffineTransform.identity
            
        }, completion: { finished in
            
            let subViews = toView?.subviews
            for subview in subViews!{
                if (subview.tag == 124) {
                    subview.removeFromSuperview()
                }
            }
            tabBarController.selectedIndex = toIndex!
            self.view.isUserInteractionEnabled = true
            
        })
        
        return true
    }
    
}
