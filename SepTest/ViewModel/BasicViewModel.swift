//
//  BasicViewModel.swift
//  SepTest
//
//  Created by Armin on 3/4/22.
//

import UIKit

class BasicViewModel: NSObject {

    static let shared = BasicViewModel()
    
    func userCredential(completion: APICompletionHandler?)
    {
        var params = [String:AnyObject]()
        params["grant_type"] = grant_type as AnyObject
        params["client_id"] = client_id as AnyObject
        params["client_secret"] = client_secret as AnyObject
        
        Requests.shared.request(Router.token_creation(params: params), completion: { (json) in
            
            let loginObj = TokenObject(json)
            if loginObj.accessToken != ""
            {
                completion!(true, loginObj as TokenObject? as AnyObject?)
            }
            else {
                let errors = Errors(fromJson: json)
                completion!(false, errors.errors?[0] as ErrorObject? as AnyObject?)
            }
           
        }) { (error) in
            
            completion!(false, error.localizedDescription as AnyObject?)
            debugPrint(error)
        }
    }
    
    func getFlightsData(latitude:Double?, longitude:Double?, radius:Int?, pageLimit:Int?, pageOffset:Int?, sort:String?, completion: APICompletionHandler?)
    {
        Requests.shared.request(Router.getFlights(latitude: latitude, longitude: longitude, radius: radius, pageLimit: pageLimit, pageOffset: pageOffset, sort: sort), completion: { (json) in
            
            let result = FlightsData(fromJson: json)
            
            if result.data != nil && result.data?.count ?? 0 > 0 {
                
                completion!(true, result.data as [FlightObject]? as AnyObject?)
                
            } else {
                if result.meta?.count ?? 0 > 0
                {
                    return
                }
                else {
                    let errors = Errors(fromJson: json)
                    completion!(false, errors.errors?[0] as ErrorObject? as AnyObject?)
                    
                }
            }
        }) { (error) in
            
            completion!(false, error.localizedDescription as AnyObject?)
            debugPrint(error)
        }
    }

}
