//
//  UserData.swift
//  CineFilm
//
//  Created by Armin on 8/22/20.
//  Copyright © 2020 Armin. All rights reserved.
//

import Foundation
import KeychainAccess

class UserData:NSObject {
    static let shared = UserData()
    var keychain:Keychain
    
    override init() {
        
        self.keychain = Keychain(service: "com.sep.septest")
        super.init()
    }
    
    func storeAccessToken(_ token: String) {
        
        self.keychain["access_token"] = token
    }
    
    var accessToken:String? {
        
        return self.keychain[string:"access_token"]
    }
    
    func storeAccessTokenType(_ token: String) {
        
        self.keychain["token_type"] = token
    }
    
    var accessTokenType:String? {
        
        return self.keychain[string:"token_type"]
    }
    
//    func storeUserProfileID(_ token: String) {
//
//        self.keychain["user_id"] = token
//    }
//
//    var accessUserProfileID:String? {
//
//        return self.keychain[string:"user_id"]
//    }
    
    func tempStoreAccessToken(_ token: String) {
           
           self.keychain["temp_access_token"] = token
       }
       
       var tempAccessToken:String? {
           
           return self.keychain[string:"temp_access_token"]
       }
    
    func storeRefreshToken(_ refreshToken: String) {
        self.keychain["refresh_token"] = refreshToken

    }
    
    var refreshToken: String? {
        return self.keychain[string: "refresh_token"]
    }
    
    func storeUserName(_ userName: String) {
        self.keychain["user_name"] = userName
        
    }
    
    var userName: String? {
        return self.keychain[string: "user_name"]
    }
    
    func storeUserID(_ userID: String) {
        self.keychain["user_id"] = userID
        
    }
    
    var userID: String? {
        return self.keychain[string: "user_id"]
    }
    
    func storeExpirtionTime(_ expTime: String) {
        self.keychain["exp_time"] = expTime
        
    }
    
    var expiretionTime: String? {
        return self.keychain[string: "exp_time"]
    }
    
    
    func storeCurrentLanguage(_ language: String)
    {
        UserDefaultsHelper.shared.save(key: "current_language", value: language)
    }
    
    var currentLanguage:String? {
        return UserDefaultsHelper.shared.get(key: "current_language")
    }
  
}


class UserDefaultsHelper {
    
    static var shared = UserDefaultsHelper()
    
    func save(key: String, value: String) {
        UserDefaults.standard.set(value, forKey: key)
    }
    
    func get(key: String) -> String {
        
        return UserDefaults.standard.string(forKey: key) ?? ""
    }
    
    func save(key: String, value: Int) {
        UserDefaults.standard.set(value, forKey: key)
    }
    
    func get(key: String) -> Int {
        
        return UserDefaults.standard.integer(forKey: key)
    }
    
    func save(key: String, value: Bool) {
        UserDefaults.standard.set(value, forKey: key)
    }
    
    func get(key: String) -> Bool {
        
        return UserDefaults.standard.bool(forKey: key)
    }
}
