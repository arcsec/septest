//
//  Oauth.swift
//  Etka Stores
//
//  Created by Armin on 11/16/18.
//  Copyright © 2018 Armin Rassadi. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON

class OAuth2Handler: RequestAdapter, RequestRetrier {
    
    private typealias RefreshCompletion = (_ succeeded: Bool, _ accessToken: String?, _ refreshToken: String?) -> Void
    
    private let sessionManager: SessionManager = {
        let configuration = URLSessionConfiguration.default
        configuration.httpAdditionalHeaders = SessionManager.defaultHTTPHeaders
        
        return SessionManager(configuration: configuration)
    }()
    
    private let lock = NSLock()
    
    var loginObj:TokenObject!
    
    
    private var clientID: String
    private var baseURLString: String
    private var accessToken: String
    private var refreshToken: String
    
    private var isRefreshing = false
    private var requestsToRetry: [RequestRetryCompletion] = []
    
    // MARK: - Initialization
    
    public init(clientID: String, baseURLString: String, accessToken: String, refreshToken: String) {
        self.clientID = clientID
        self.baseURLString = baseURLString
        self.accessToken = accessToken
        self.refreshToken = refreshToken
    }
    
    // MARK: - RequestAdapter
    
    func adapt(_ urlRequest: URLRequest) throws -> URLRequest {
        if let urlString = urlRequest.url?.absoluteString, urlString.hasPrefix(baseURLString) {
            var urlRequest = urlRequest
            
            if accessToken != "" {
                urlRequest.setValue("bearer " + accessToken, forHTTPHeaderField: "Authorization")
                
            }
            return urlRequest
        }
        
        return urlRequest
    }
    
    // MARK: - RequestRetrier
    
    func should(_ manager: SessionManager, retry request: Request, with error: Error, completion: @escaping RequestRetryCompletion) {
        lock.lock() ; defer { lock.unlock() }
        
        if let response = request.task?.response as? HTTPURLResponse, response.statusCode == 401 {
            requestsToRetry.append(completion)
            
            if !isRefreshing {
                refreshTokens { [weak self] succeeded, accessToken, refreshToken in
                    guard let strongSelf = self else { return }
                    
                    strongSelf.lock.lock() ; defer { strongSelf.lock.unlock() }
                    
                    if let accessToken = accessToken, let refreshToken = refreshToken {
                        strongSelf.accessToken = accessToken
                        strongSelf.refreshToken = refreshToken
                    }
                    
                    strongSelf.requestsToRetry.forEach { $0(succeeded, 0.0) }
                    strongSelf.requestsToRetry.removeAll()
                }
            }
        } else {
            completion(false, 0.0)
        }
    }
    
    // MARK: - Private - Refresh Tokens
    
    private func refreshTokens(completion: @escaping RefreshCompletion) {
        guard !isRefreshing else { return }
        
        isRefreshing = true
        
        let urlString = "\(baseURLString)/v1/security/oauth2/token"
        
        let parameters: [String: Any] = [
            "access_token": accessToken,
            "client_id": clientID,
            "grant_type": grant_type,
        ]
        
        sessionManager.request(urlString, method: .post, parameters: parameters, encoding: URLEncoding.default)
            .responseJSON { [weak self] response in
                guard let strongSelf = self else { return }
                
                switch response.result {
                case .success(let value):
                    
                    let json = JSON(value)

                    self?.loginObj = TokenObject(json)
                     
                    
                    Utility.shared.saveUserTokenData(accessToken: self?.loginObj.accessToken ?? "", expireTime: self?.loginObj.expiresIn ?? 0, userID: self?.loginObj.client_id ?? "",userName: self?.loginObj.userName ?? "")

                    completion(true, self?.loginObj.accessToken, "")
                    
                case .failure(_):
                    
                    completion(false, nil, nil)
                }
                strongSelf.isRefreshing = false
        }
    }

    func requestLogin() {
        DispatchQueue.main.async {
            print("back to login")
            
            Utility.shared.saveUserTokenData(accessToken: "", expireTime: 0, userID: "", userName: "")

            
            var storyboardId : String
            var storyboard :UIStoryboard
            var initViewController: UIViewController
            let appDelegate = UIApplication.shared.delegate as! AppDelegate

            storyboardId  = "LunchScreenVC";
            storyboard = UIStoryboard.init(name: "Main", bundle: nil)
            
            initViewController = storyboard.instantiateViewController(withIdentifier: storyboardId) as! LunchScreenViewController
            
            appDelegate.window?.rootViewController = initViewController;
            appDelegate.window?.makeKeyAndVisible()
        }

    }
    
   


}

