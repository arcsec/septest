//
//  Router.swift
//  VOD
//
//  Created by Armin on 8/9/20.
//  Copyright © 2020 Armin. All rights reserved.
//

import UIKit
import Alamofire

enum Router {
    
    static var baseUrlString:String
    {
        return "https://test.api.amadeus.com"
    }
    
    private var baseUrl:URL
    {
        return URL(string: Router.baseUrlString)!
    }
    
    static var activeUserHeader : HTTPHeaders = [
        "accept": "Application/json",
        "Authorization" : "Bearer " + (UserData.shared.accessToken ?? "")
    ]
    
    case token_creation(params:Dictionary<String, Any>)

    case getFlights(latitude:Double?, longitude:Double?, radius:Int?, pageLimit:Int?, pageOffset:Int?, sort:String?)
    
    var endUrl:URL {
        
        switch self {
            
        case .getFlights(let latitude, let longitude, let radius, let pageLimit, let pageOffset, let sort):
            
            let queryItems:[URLQueryItem] = [NSURLQueryItem(name: "latitude", value: "\(latitude ?? 32.4279)") as URLQueryItem,
                                             NSURLQueryItem(name: "longitude", value: "\(longitude ?? 53.6880)") as URLQueryItem,
                                             NSURLQueryItem(name: "radius", value: "\(radius ?? 1000)") as URLQueryItem,
//                                             NSURLQueryItem(name: "page_limit", value: "\(pageLimit ?? 20)") as URLQueryItem,
//                                             NSURLQueryItem(name: "page_offset", value: "\(pageOffset ?? 0)") as URLQueryItem,
                                             NSURLQueryItem(name: "sort", value: sort ?? "\(SortTypes.relevance)") as URLQueryItem
            ]
            let urlComps = NSURLComponents(string: "\(baseUrl)"+"/v1/reference-data/locations/airports")
            urlComps?.queryItems = queryItems
            let url:URL = urlComps?.url ?? URL(string: "problem")!
            return url
            
            
        case .token_creation(_):
            return self.baseUrl.appendingPathComponent("/v1/security/oauth2/token")
         
        }
    }
    
    var method: HTTPMethod {
        
        switch self {
            
        case .token_creation:
            return HTTPMethod.post
            
        case .getFlights:
            return HTTPMethod.get

        }
    }
    
    var parameters:Parameters? {
        
        switch self {
    
        case .getFlights:
            return nil

        case .token_creation(let params):
            return params

        }
    }
    
    var encoding: ParameterEncoding {
        
        switch self {
        case .getFlights:
            
            return JSONEncoding.default
            
        case .token_creation:
            return URLEncoding.httpBody
            
        }
    
    }
    
    var headers: HTTPHeaders? {
        
        let tokenType:String = UserData.shared.accessTokenType ?? "Bearer"
        let accessToken:String = UserData.shared.accessToken ?? ""
        
        switch self {
            
            
        case .token_creation:
            return ["Content-Type": "application/x-www-form-urlencoded"]

        case .getFlights:
            
            return ["Content-Type":"application/json", "Authorization" :  tokenType + " " + accessToken]
        }
        
    }
    
    var validate:[Int] {
        
        switch self {
        case .token_creation, .getFlights:
            
            return [200]
            
        }
        
    }
    
}
