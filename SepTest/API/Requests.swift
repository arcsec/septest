//
//  Requests.swift
//  Hamyan
//
//  Created by Armin on 9/27/18.
//  Copyright © 2018 Armin. All rights reserved.

import Foundation
import Alamofire
import SwiftyJSON
import NVActivityIndicatorView

typealias PlainCompletionHandler = (_ success:Bool) -> Void
typealias APICompletionHandler = (_ success:Bool,_ obj: AnyObject?) -> Void
typealias UploadCompletionHandler = (_ uploadReq: Alamofire.UploadRequest?) -> Void
typealias DownloadCompletionHandler = (_ uploadReq: Alamofire.DownloadRequest?) -> Void


class Requests {
    
    static let shared = Requests()
    var sessionManager: SessionManager = {
        let serverTrustPolicies: [String: ServerTrustPolicy] = [
            "https://test.api.amadeus.com": .disableEvaluation,
            "test.api.amadeus.com": .disableEvaluation,

        ]
        let configuration = URLSessionConfiguration.default
        configuration.httpAdditionalHeaders = Alamofire.SessionManager.defaultHTTPHeaders
        let manager = Alamofire.SessionManager(
            configuration: URLSessionConfiguration.default,
            serverTrustPolicyManager: ServerTrustPolicyManager(policies: serverTrustPolicies)
        )
        return SessionManager(configuration: configuration)
    }()
    
    let retrier = OAuth2Handler(clientID: client_id, baseURLString: BASEURL, accessToken: UserData.shared.accessToken ?? "", refreshToken: UserData.shared.refreshToken ?? "")
    
    init() {
        let statStr = UserData.shared.accessToken
        if statStr != nil && statStr != "" {
            sessionManager.adapter = retrier
            sessionManager.retrier = retrier
        }
    }
    
    func request(_ router: Router, indicator:Bool = false, completion: @escaping (JSON) -> (), onError: @escaping (Error) -> ()) {
        print(router.encoding)
        print(router.endUrl)
        if indicator {
            
            NVActivityIndicatorPresenter.sharedInstance.startAnimating(ActivityData(type: .ballRotateChase), nil)
        
        }
            sessionManager.request(router.endUrl, method: router.method, parameters: router.parameters, encoding: router.encoding, headers: router.headers)
                .validate(statusCode: router.validate)
                .responseJSON { (response) in
                    switch response.result {
                    case .success(let value):
                        NVActivityIndicatorPresenter.sharedInstance.stopAnimating(nil)
                        
                        let json = JSON(value)
                        completion(json)
                    case .failure(let error):
                        NVActivityIndicatorPresenter.sharedInstance.stopAnimating(nil)

                        print(error)
                        onError(error)
                    }
            }
        
    }
}
