//
//  LunchScreenViewController.swift
//
//  Created by Armin on 10/1/20.
//  Copyright © 2020 Armin@Armin. All rights reserved.
//

import UIKit
import SwiftyDrop

class LunchScreenViewController: UIViewController {
    
    static let shared = LunchScreenViewController()
    var loginObj:TokenObject!

    
    override func viewDidLoad() {
        super.viewDidLoad()

        let statStr = UserData.shared.accessToken
        if statStr != nil && statStr != "" {
            self.goToMainController()
        }
        else {
            login()
        }
    }
    
 
    func goToMainController()
    {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        
        var storyboardId : String
        var storyboard :UIStoryboard
        var initViewController: TabBarViewController
        
        storyboardId = "navRootVCID"
        storyboard = UIStoryboard.init(name: "Main", bundle: nil)
        
        initViewController = storyboard.instantiateViewController(withIdentifier: storyboardId) as! TabBarViewController
        
        appDelegate.window?.rootViewController = initViewController;
        appDelegate.window?.makeKeyAndVisible()
        
    }
    
    func login()
    {
        BasicViewModel.shared.userCredential { success, obj in
            
            if !success
            {
                let errorObj:ErrorObject? = obj as? ErrorObject
                let errorMsg:String = "Error \(errorObj?.status ?? 0) - \(errorObj?.title ?? "") (\(errorObj?.detail ?? ""))"
                Drop.down(errorMsg , state: CustomDrop.failure)
            }
            else
            {
                self.loginObj = obj as? TokenObject
                Utility.shared.saveUserTokenData(accessToken: self.loginObj.accessToken ?? "", expireTime: self.loginObj.expiresIn ?? 0, userID: self.loginObj.client_id ?? "",userName: self.loginObj.userName ?? "")
                
                self.goToMainController()
            }
        }
    }
    
}
