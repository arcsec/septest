//
//  AppDelegate.swift
//  SepTest
//
//  Created by Armin on 3/3/22.
//

import UIKit
import IQKeyboardManagerSwift

@main
class AppDelegate: UIResponder, UIApplicationDelegate {


    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
        // Override point for customization after application launch.
        
        IQKeyboardManager.shared.enable = true
        IQKeyboardManager.shared.enableAutoToolbar = true
        
        
        var storyboardId : String
        var storyboard :UIStoryboard
        var initViewController: LunchScreenViewController
        
        storyboardId  = "LunchScreenVC";
        storyboard = UIStoryboard.init(name: "Main", bundle: nil)
        
        initViewController = storyboard.instantiateViewController(withIdentifier: storyboardId) as! LunchScreenViewController
        
        
        self.window?.rootViewController = initViewController;
        self.window?.makeKeyAndVisible()
        
        
        return true
    }

}

