//
//  BaseCell.swift
//
//  Created by Armin on 10/3/18.
//  Copyright © 2018 Armin. All rights reserved.
//

import UIKit

class BaseCell<U>: UITableViewCell {

    var item: U!
}
