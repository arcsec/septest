//
//  BaseTableView.swift
//
//  Created by Armin on 10/3/18.
//  Copyright © 2018 Armin. All rights reserved.
//

import UIKit

class BaseTableView<T: UITableViewCell>: UITableView {

    let cellID = "BaseCellId"
    
    override init(frame: CGRect, style: UITableView.Style) {
        super.init(frame: frame, style: .plain)
        
        register(T.self, forCellReuseIdentifier: cellID)
        contentInset = UIEdgeInsets.zero
        tableFooterView = UIView()
        separatorColor = UIColor(rgb: 0xEFEFEF, a: 1.0)
        rowHeight = UITableView.automaticDimension
        estimatedRowHeight = 200
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}
