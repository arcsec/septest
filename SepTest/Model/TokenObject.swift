//
//  TokenObject.swift
//  Etka Stores
//
//  Created by Armin Rassadi on 9/8/17.
//  Copyright © 2017 Armin Rassadi. All rights reserved.
//

import Foundation
import SwiftyJSON

struct TokenObject{
    
    var type : String!
    var client_id : String!
    var userName : String!
    var accessToken : String!
    var state : String!
    var expiresIn : Int!
    var tokenType : String!
    var application_name : String!

    
    /**
     * Instantiate the instance using the passed dictionary values to set the properties values
     */
    init(_ json: JSON) {
        type = json["type"].stringValue
        client_id = json["client_id"].stringValue
        userName = json["username"].stringValue
        accessToken = json["access_token"].stringValue
        state = json["state"] .stringValue
        expiresIn = json["expires_in"].intValue
        tokenType = json["token_type"].stringValue
        application_name = json["application_name"].stringValue

    }
    
   
}
