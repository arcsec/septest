//
//  FlightObject.swift
//  SepTest
//
//  Created by NGN on 3/4/22.
//

import Foundation
import SwiftyJSON

// MARK: - FlightsData
class FlightsData {
    var meta: Meta?
    var data: [FlightObject]?
    
    init(fromJson json: JSON){
        if json.isEmpty{
            return
        }
        
        let metaJson = json["meta"]
        if !metaJson.isEmpty{
            meta = Meta(fromJson: metaJson)
        }
        
        data = [FlightObject]()
        let errorArray = json["data"].arrayValue
        self.data = errorArray.map {
            FlightObject(fromJson: $0)
        }
        
    }
}

// MARK: - FlightObject
class FlightObject {
    var type: String?
    var subType: String?
    var name, detailedName: String?
    var timeZoneOffset: String?
    var iataCode: String?
    var geoCode: GeoCode?
    var address: Address?
    var distance: Distance?
    var analytics: Analytics?
    var relevance: Double?
    
    /**
     * Instantiate the instance using the passed json values to set the properties values
     */
    init(fromJson json: JSON){
        if json.isEmpty{
            return
        }
        type = json["type"].stringValue
        subType = json["subType"].stringValue
        name = json["name"].stringValue
        timeZoneOffset = json["timeZoneOffset"].stringValue
        iataCode = json["iataCode"].stringValue
        relevance = json["relevance"].doubleValue
        
        let geoCodeJson = json["geoCode"]
        if !geoCodeJson.isEmpty{
            geoCode = GeoCode(fromJson: geoCodeJson)
        }
        
        let addressJson = json["address"]
        if !addressJson.isEmpty{
            address = Address(fromJson: addressJson)
        }
        
        let distanceJson = json["distance"]
        if !distanceJson.isEmpty{
            distance = Distance(fromJson: distanceJson)
        }
        
        let analyticsJson = json["analytics"]
        if !analyticsJson.isEmpty{
            analytics = Analytics(fromJson: analyticsJson)
        }
    }
    
}

// MARK: - Address
class Address {
    var cityName, cityCode: String?
    var countryName: String?
    var countryCode: String?
    var regionCode: String?
    
    init(fromJson json: JSON){
        if json.isEmpty{
            return
        }
        cityName = json["cityName"].stringValue
        cityCode = json["cityCode"].stringValue
        countryName = json["countryName"].stringValue
        countryCode = json["countryCode"].stringValue
        regionCode = json["regionCode"].stringValue
        
    }
}

// MARK: - GeoCode
class GeoCode {
    var latitude, longitude: Double?
    
    
    init(fromJson json: JSON){
        if json.isEmpty{
            return
        }
        latitude = json["latitude"].doubleValue
        longitude = json["longitude"].doubleValue
        
    }
}


// MARK: - Analytics
class Analytics {
    var flights: Flights?
    var travelers : Travelers?
    
    init(fromJson json: JSON){
        if json.isEmpty{
            return
        }
        
        let flightsJson = json["flights"]
        if !flightsJson.isEmpty{
            flights = Flights(fromJson: flightsJson)
        }
        
        let travelersJson = json["travelers"]
        if !travelersJson.isEmpty{
            travelers = Travelers(fromJson: flightsJson)
        }
        
    }
    
}

// MARK: - Flights
class Flights {
    var score: Int?
    
    init(fromJson json: JSON){
        if json.isEmpty{
            return
        }
        score = json["score"].intValue
    }
}


// MARK: - Travelers
class Travelers {
    var score: Int?
    
    init(fromJson json: JSON){
        if json.isEmpty{
            return
        }
        score = json["score"].intValue
    }
}

// MARK: - Distance
class Distance {
    
    var value: Int?
    var unit: String?
    
    init(fromJson json: JSON){
        if json.isEmpty{
            return
        }
        
        self.value = json["value"].intValue
        self.unit = json["unit"].stringValue
        
    }
}


// MARK: - Meta
class Meta {
    var count: Int?
    var links: Links?
    
    
    init(fromJson json: JSON){
        if json.isEmpty{
            return
        }
        
        let linksJson = json["Links"]
        if !linksJson.isEmpty{
            links = Links(fromJson: linksJson)
        }
        
        count = json["count"].intValue
    }
}

// MARK: - Links
class Links {
    
    var link_self, next, last: String?
    
    /**
     * Instantiate the instance using the passed json values to set the properties values
     */
    init(fromJson json: JSON){
        if json.isEmpty{
            return
        }
        link_self = json["self"].stringValue
        next = json["next"].stringValue
        last = json["last"].stringValue
        
    }
}


// MARK: - Errors
class Errors {
    var errors: [ErrorObject]?
    
    init(fromJson json: JSON){
        if json.isEmpty{
            return
        }
      
        errors = [ErrorObject]()
        let errorArray = json["errors"].arrayValue
        self.errors = errorArray.map {
            ErrorObject(fromJson: $0)
        }
    }
}


// MARK: - ErrorObject
class ErrorObject {
    var source: Source?
    var status, code: Int?
    var title, detail: String?

    init(fromJson json: JSON){
        if json.isEmpty{
            return
        }
        
        let sourceJson = json["source"]
        if !sourceJson.isEmpty{
            source = Source(fromJson: sourceJson)
        }
        
    }
}


// MARK: - Source
class Source {

    var parameter, example: String?
    
    init(fromJson json: JSON){
        if json.isEmpty{
            return
        }
        
        parameter = json["parameter"].stringValue
        example = json["example"].stringValue
    }
}



// MARK: - SortTypes
enum SortTypes: String {
    case relevance = "relevance", distance = "distance",
         flight_score = "analytics.flights.score", travelers_score = "analytics.travelers.score "
    
    static let all = [relevance, distance, flight_score, travelers_score]
}

